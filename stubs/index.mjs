import {getPostList} from './templates/postList/postList.mjs';
import { getPost} from './templates/postList/post.mjs';

export const stubs = {
    "/posts": getPostList,
    "/posts/1": getPost,
};
