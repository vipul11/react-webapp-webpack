/* eslint-disable max-len */
import React, { memo } from 'react';
import Atom from '../../assets/images/Atom.png';

const Sample2Container = memo(() => {
  return (
    <div className="card">
      <div className="card-header bg-primary">
        <div className="card-title">
          <img src={Atom}></img> &nbsp; Sample2
        </div>
      </div>
      <div className="card-body">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 96 96">
          <path d="M96 90.8V5.5l.1-.1-.1-.1V0H-.2v96h5.1l.3.3.3-.3h85l.3.3.3-.3H96v-5.2zM47.9 42.7L12.8 7.6H83L47.9 42.7zm40.5-29.6v70.1l-35-35 35-35.1zm-45.8 35L7.8 82.9V13.4l34.8 34.7zm5.3 5.4l34.8 34.8H13.1l34.8-34.8z" />
        </svg>
      </div>
    </div>
  );
});

export { Sample2Container };
