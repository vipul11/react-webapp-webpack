const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const paths = require('../../paths');
const copy = require('./webpack.common.config');

module.exports = {
  mode: 'development',
  devServer: {
    historyApiFallback: true,
    contentBase: paths.resolveFromRoot('dist'),
    open: true,
    compress: true,
    hot: true,
    port: 8080
  },

  devtool: 'inline-source-map',
  entry: paths.resolveFromRoot('src/index.js'),
  resolve: {
    modules: [paths.resolveFromRoot('src'), 'node_modules'],
    extensions: ['.js', '.jsx', '.json', '.mjs']
  },
  module: {
    rules: [
      {
        test: /\.(mjs|js|jsx)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        test: /\.(css|scss|sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader'
          },
          'resolve-url-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack']
      },
      {
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource'
      }
    ]
  },
  output: {
    filename: '[name].[contenthash].js',
    path: paths.resolveFromRoot('dist'),
    publicPath: '/'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    }),
    new CopyPlugin({
      patterns: [...copy]
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash].css',
      chunkFilename: '[name].css'
    }),
    new HTMLWebpackPlugin({
      inject: true,
      template: paths.resolveFromRoot('src/index.html')
    }),
    new ReactRefreshWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
};
